package swim.bmi

import io.kotlintest.shouldThrow
import io.kotlintest.specs.StringSpec
import org.junit.Assert
import swim.bmi.logic.BmiForLbIn

class BmiForLbInTest :  StringSpec() {

    init {
        "for valid data " {
            val bmi = BmiForLbIn(130,60)
            bmi.countBmi() shouldBeAround 25.386
        }

        "for min valid data" {
            val bmi = BmiForLbIn(44,20)
            bmi.countBmi() shouldBeAround 77.33
        }

        "for max valid data" {
            val bmi = BmiForLbIn(2000,160)
            bmi.countBmi() shouldBeAround 54.921
        }

        "for invalid data"{
            val bmi = BmiForLbIn(-1,0)
            shouldThrow<IllegalArgumentException> {bmi.countBmi() }
        }

        "for invalid mass"{
            val bmi = BmiForLbIn(-1,60)
            shouldThrow<IllegalArgumentException> {bmi.countBmi() }
        }

        "for invalid height"{
            val bmi = BmiForLbIn(50,0)
            shouldThrow<IllegalArgumentException> {bmi.countBmi() }
        }
    }

    infix fun Double.shouldBeAround(value: Double){
        Assert.assertEquals(value, this, 0.001)
    }



//    @Test
//    fun for_valid_data_should_return_bmi() {
//        val bmi = BmiForLbIn(130, 60)
//        Assert.assertEquals(25.386, bmi.countBmi(), 0.001)
//    }

//    @Test
//    fun for_invalid_data() {
//        val bmi = BmiForLbIn(0,100)
//
//    }

}