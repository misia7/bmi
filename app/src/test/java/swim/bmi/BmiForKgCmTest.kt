package swim.bmi

import io.kotlintest.shouldThrow
import io.kotlintest.specs.StringSpec
import org.junit.Assert
import swim.bmi.logic.BmiForKgCm

class BmiForKgCmTest : StringSpec() {
    init {
        "for valid data" {
            val bmi = BmiForKgCm(55,160)
            bmi.countBmi() shouldBeAround 21.484
        }

        "for min valid data" {
            val bmi = BmiForKgCm(20,40)
            bmi.countBmi() shouldBeAround 125.0
        }

        "for max valid data" {
            val bmi = BmiForKgCm(1000,400)
            bmi.countBmi() shouldBeAround 62.5
        }

        "for invalid data"{
            val bmi = BmiForKgCm(0,-5)
            shouldThrow<IllegalArgumentException> {bmi.countBmi() }
        }

        "for invalid mass"{
            val bmi = BmiForKgCm(0,50)
            shouldThrow<IllegalArgumentException> {bmi.countBmi() }
        }

        "for invalid height"{
            val bmi = BmiForKgCm(50,-5)
            shouldThrow<IllegalArgumentException> {bmi.countBmi() }
        }
    }

    infix fun Double.shouldBeAround(value: Double){
        Assert.assertEquals(value, this, 0.001)
    }
}