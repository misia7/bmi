package swim.bmi.logic

class BmiForKgCm(var mass: Int, var height: Int) : Bmi {

    private val massRange = 20..1000
    private val heightRange = 40..400

    override fun countBmi(): Double {
        require(mass in massRange) {"mass in 20..1000"}
        require(height in heightRange) {"height in 40..400"}
        val bmi : Double = mass * 10000.0 / (height * height)
        return bmi
    }
}