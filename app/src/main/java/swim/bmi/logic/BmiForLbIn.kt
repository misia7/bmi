package swim.bmi.logic

class BmiForLbIn(var mass: Int, var height: Int) : Bmi {

    private val massRange = 44..2000
    private val heightRange = 20..160

    override fun countBmi(): Double {
        require(mass in massRange) {"mass in 44..2000"}
        require(height in heightRange) {"height in 20..160"}
        val bmi : Double = mass * 703.0 / (height * height)
        return bmi
    }
}