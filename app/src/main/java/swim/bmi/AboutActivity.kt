package swim.bmi

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import kotlinx.android.synthetic.main.about_layout.*

class AboutActivity: AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.about_layout)
        setSupportActionBar(toolbar_about as Toolbar?)

        switchColor.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked)
                switchColor.setTextColor(ContextCompat.getColor(this, R.color.colorAccent))
            else
                switchColor.setTextColor(ContextCompat.getColor(this, R.color.colorPrimaryDark))
        }
    }
}