package swim.bmi

import java.util.*

class BmiElement (val mass: Int, val height: Int, val bmi: Double, val massUnits: String, val heightUnits: String, val text: String, val color: Int) {
    val date = Calendar.getInstance().time
}