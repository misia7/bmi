package swim.bmi

import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import java.text.SimpleDateFormat
import java.util.*


class HistoryAdapter(private val data : MutableList<BmiElement>) : RecyclerView.Adapter<HistoryAdapter.ViewHolder>() {


    class ViewHolder(View: View) : RecyclerView.ViewHolder(View) {
        val mass : TextView = itemView.findViewById(R.id.tv_mass)
        val height : TextView = itemView.findViewById(R.id.tv_height)
        val massUnits : TextView = itemView.findViewById(R.id.tv_mass_units)
        val heightUnits : TextView = itemView.findViewById(R.id.tv_height_units)
        val date : TextView = itemView.findViewById(R.id.tv_date)
        val bmiResult : TextView = itemView.findViewById(R.id.tv_bmi_result)
        val bmiText : TextView = itemView.findViewById(R.id.tv_bmi_text)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val View = LayoutInflater.from(parent.context).inflate(R.layout.bmi_element, parent, false)
        return ViewHolder(View)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val elem : BmiElement = data[getItemCount()-1-position]

        holder.mass.text = elem.mass.toString()
        holder.height.text = elem.height.toString()
        holder.massUnits.text = elem.massUnits
        holder.heightUnits.text = elem.heightUnits
        holder.date.text = SimpleDateFormat("dd.MM.yyyy  HH:mm:ss", Locale.getDefault()).format(elem.date)
        holder.bmiResult.setTextColor(elem.color)
        holder.bmiText.setTextColor(elem.color)
        holder.bmiResult.text = "%.2f".format(elem.bmi)
        holder.bmiText.text = elem.text

        if(elem.text == MainActivity.MyContext.getString(R.string.text_normal))
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            holder.itemView.setBackgroundColor(MainActivity.MyContext.getColor(R.color.colorPrimary))
        }
    }

    override fun getItemCount() = data.size

}