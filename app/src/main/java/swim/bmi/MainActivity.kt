package swim.bmi

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

import kotlinx.android.synthetic.main.activity_main.*
import swim.bmi.logic.BmiForKgCm
import swim.bmi.logic.BmiForLbIn
import java.lang.Double.MAX_VALUE


class MainActivity : AppCompatActivity() {

    companion object {
        const val RESULT_BMI_KEY = "resultBMI"
        const val RESULT_TEXT_KEY = "resultText"
        const val UNITS_KEY = "isMetricUnits"
        const val BMI_KEY = "countedBmi"
        const val INFO_KEY = "isInfoVisible"
        const val LIST_KEY = "bmiResultsList"
        lateinit var MyContext: Context
    }

    private var bmiList: MutableList<BmiElement> = mutableListOf()
    private var isMetric = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar as Toolbar?)

        MyContext = this

        infoB.setOnClickListener {
            val intent = Intent(this, InfoActivity::class.java)
            intent.putExtra(RESULT_BMI_KEY, resultNumber.text)
            intent.putExtra(RESULT_TEXT_KEY, resultText.text)
            startActivity(intent)
        }

        countB.setOnClickListener {
            try {
                val mass = Integer.parseInt(massET.text.toString())
                val height = Integer.parseInt(heightET.text.toString())

                val bmiResult: Double
                val massUnits : String
                val heightUnits : String

                try {
                    if ((toolbar as Toolbar?)?.menu?.findItem(R.id.switch_metric_units)?.isChecked!!) {
                        // if is metric
                        val bmiCount = BmiForKgCm(mass, height)
                        bmiResult = bmiCount.countBmi()
                        massUnits = "kg"
                        heightUnits = "cm"
                    } else {
                        // if not metric
                        val bmiCount = BmiForLbIn(mass, height)
                        bmiResult = bmiCount.countBmi()
                        massUnits = "Lb"
                        heightUnits = "In"
                    }
                    setBmiText(bmiResult)
                    addToHistory(mass, height, bmiResult, massUnits, heightUnits)
                    infoB.isVisible = true

                } catch (e: Exception) {
                    clearAllFields()
                    Toast.makeText(this@MainActivity, "${e.message}", Toast.LENGTH_SHORT).show()
                }
            } catch (e: Exception) {
                clearAllFields()
                Toast.makeText(this@MainActivity, getString(R.string.text_enter_value), Toast.LENGTH_SHORT).show()
            }
        }
    }

    // menu z kropkami
    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)

        menu.findItem(R.id.switch_metric_units)?.isChecked = isMetric

        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.switch_metric_units -> {
                item.isChecked = !item.isChecked
                setText(item.isChecked)
                clearAllFields()
            }
            R.id.about -> {
                startActivity(Intent(this, AboutActivity::class.java))
            }
            R.id.history -> {
                val intent = Intent(this, HistoryActivity::class.java)
                val data = Gson().toJson(bmiList)
                intent.putExtra(LIST_KEY, data)
                startActivity(intent)
            }
        }
        return true
    }

    override fun onPause() {
        super.onPause()

        var sp : SharedPreferences = this.getPreferences(Context.MODE_PRIVATE)
        var ed = sp.edit()
        ed.putString(LIST_KEY, Gson().toJson(bmiList))
        ed.commit()
    }

    override fun onResume() {
        super.onResume()

        val listType = object : TypeToken<MutableList<BmiElement>>() {}.type

        var sp : SharedPreferences = this.getPreferences(Context.MODE_PRIVATE)
        val textBmiHistory = sp.getString(LIST_KEY, "")
        if(textBmiHistory != "") {
            bmiList = Gson().fromJson(textBmiHistory, listType)
        }

    }

    override fun onSaveInstanceState(outState: Bundle?) {
        outState?.putCharSequence(BMI_KEY, resultNumber.text)
        outState?.putBoolean(INFO_KEY, infoB.isVisible)
        outState?.putBoolean(UNITS_KEY, (toolbar as Toolbar?)?.menu?.findItem(R.id.switch_metric_units)?.isChecked!!)

        super.onSaveInstanceState(outState)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle?) {
        super.onRestoreInstanceState(savedInstanceState)

        isMetric = savedInstanceState!!.getBoolean(UNITS_KEY)
        setText(isMetric)

        resultNumber.text = savedInstanceState.getCharSequence(BMI_KEY)
        if (resultNumber.text != "")
            setBmiText(resultNumber.text.toString().toDouble())

        infoB.isVisible = savedInstanceState.getBoolean(INFO_KEY)
    }

    private fun setBmiText(bmiResult: Double) {
        val color = getColor(bmiResult)
        val text = getText(bmiResult)
        setColorAndText(color, text)

        var value = "%.2f".format(bmiResult)
        resultNumber.text = value.replace(",", ".")
    }


    private fun getColor(bmiResult: Double) = when (bmiResult) {
        in 0.0..18.5 -> R.color.colorUnderweight
        in 18.5..25.0 -> R.color.colorNormal
        in 25.0..30.0 -> R.color.colorOverweight
        in 30.0..35.0 -> R.color.colorObese
        in 35.0..MAX_VALUE -> R.color.colorExtremelyObese
        else -> R.color.colorNone
    }

    private fun getText(bmiResult: Double): Int = when (bmiResult) {
        in 0.0..18.5 -> R.string.text_underweight
        in 18.5..25.0 -> R.string.text_normal
        in 25.0..30.0 -> R.string.text_overweight
        in 30.0..35.0 -> R.string.text_obese
        in 35.0..MAX_VALUE -> R.string.text_extremely_obese
        else -> R.string.text_empty
    }

    private fun setColorAndText(color: Int, text: Int) {
        resultNumber.setTextColor(ContextCompat.getColor(this, color))
        resultText.setTextColor(ContextCompat.getColor(this, color))
        resultText.text = getString(text)
    }

    private fun clearAllFields() {
        massET.text.clear()
        heightET.text.clear()
        resultNumber.text = ""
        resultText.text = ""
        infoB.isVisible = false
    }

    private fun setText(checked: Boolean) {
        if (checked) {
            massText.text = getString(R.string.text_mass_kg)
            heightText.text = getString(R.string.text_height_cm)
        } else {
            massText.text = getString(R.string.text_mass_lb)
            heightText.text = getString(R.string.text_height_in)
        }
    }

    private fun addToHistory(mass: Int, height: Int, bmi: Double, massUnits: String, heightUnits: String) {
        val elem = BmiElement(mass, height, bmi, massUnits, heightUnits, getString(getText(bmi)), ContextCompat.getColor(this, getColor(bmi)))
        while(bmiList.size > 9)
            bmiList.removeAt(0)
        bmiList.add(elem)
    }
}