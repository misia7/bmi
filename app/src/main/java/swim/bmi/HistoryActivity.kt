package swim.bmi

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import swim.bmi.MainActivity.Companion.LIST_KEY

class HistoryActivity : AppCompatActivity() {

    private lateinit var recyclerView: RecyclerView
    private lateinit var viewAdapter: RecyclerView.Adapter<*>
    private lateinit var viewManager: RecyclerView.LayoutManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.history_layout)

        viewManager = LinearLayoutManager(this)

        val list = intent.getStringExtra(LIST_KEY)
        val listType = object : TypeToken<MutableList<BmiElement>>() {}.type

        val data: MutableList<BmiElement>
        if (list != null)
            data = Gson().fromJson<MutableList<BmiElement>>(list, listType)
        else
            data = mutableListOf()

        viewAdapter = HistoryAdapter(data)

        recyclerView = findViewById<RecyclerView>(R.id.history_layout).apply {
            setHasFixedSize(true)
            layoutManager = viewManager
            adapter = viewAdapter
        }
    }
}