package swim.bmi

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import kotlinx.android.synthetic.main.info_layout.*

class InfoActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.info_layout)
        setSupportActionBar(toolbar_info as Toolbar?)

        val bmiNumber = intent.getStringExtra("resultBMI")
        val bmiText = intent.getStringExtra("resultText")

        when (bmiText) {
            "Underweight" -> {
                setColorAndText(R.color.colorUnderweight, R.string.description_underweight)
            }
            "Normal" -> {
                setColorAndText(R.color.colorNormal, R.string.description_normal)
            }
            "Overweight" -> {
                setColorAndText(R.color.colorOverweight, R.string.description_overweight)
            }
            "Obese" -> {
                setColorAndText(R.color.colorObese, R.string.description_obese)
            }
            "Extremely Obese" -> {
                setColorAndText(R.color.colorExtremelyObese, R.string.description_extremely_obese)
            }
        }

        bmi_number_text.text = bmiNumber
        bmi_name_text.text = bmiText
    }

    fun setColorAndText(color: Int, text: Int) {
        bmi_number_text.setTextColor(ContextCompat.getColor(this, color))
        bmi_name_text.setTextColor(ContextCompat.getColor(this, color))
        description_text.setTextColor(ContextCompat.getColor(this, color))
        description_text.text = getString(text)
    }
}